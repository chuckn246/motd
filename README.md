# MOTD

Display information when ssh-ing into the remote system.


This has been used on various Debian installs. It is untested on any other distributions.


![MOTD Screenshot](scrots/motd.png)

_Above screenshot is old version with update script which didn't really work correctly._

## Features 
* ascii art hostname
* Distribution information
* System Information
* Fortune

## Instructions

1. On the remote machine, install the following packages:

```bash
[user@remote]$ sudo apt install figlet fortune-mod fortunes{,-{bofh-excuses,debian-hints,off}}
```
* `figlet` - Creates ASCII art from text
* `fortune-mod` - Fortune cookie program
* `fortunes` - More fortunes for fortune-mod (optional)
* `fortunes-debian-hints` - Debian hints for fortune-mod (optional)
* `fortunes-bofh-excuses` - Bastard Operator From Hell Excuses for fortune-mod (optional)
* `fortunes-off` - Offensive fortunes for fortune-mod (optional)

2. Place the files from this repo in `/etc/update-motd.d` and make them executable:
```bash
[user@remote:motd]$ chmod +x 10-uname 20-sysinfo 90-fortune
[user@remote:motd]$ cp 10-uname 20-sysinfo 90-fortune /etc/update-motd.d/
```

3. Edit the following file:

```bash
[user@remote]$ sudoedit /etc/pam.d/sshd
```
* Comment out the line:
    * `session      optional    pam_motd.so noupdate`
* So it reads:
    * `# session      optional    pam_motd.so noupdate`

4. Log out then log back in and it should display the new MOTD!
```bash
[user@remote]$ exit
[user@local]$ ssh remote
[user@remote]$ SUPER COOL MOTD
```
